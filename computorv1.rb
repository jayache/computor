#!/bin/ruby
@LIMIT = 10000000000000000
@E = 2.718281828
class String
  def numeric?
    (/\d+(\.\d)?/ =~ self) != nil and Float(self) != nil rescue false
  end
end

def abs(a)
  a < 0 ? -a : a
end

def exit_error(errormsg, equation, index)
  STDERR.puts equation
  STDERR.puts "#{" " * index}^"
  STDERR.puts errormsg
  exit 1
end

def get_equations_terms(equation)
  tokens = equation.split(" ")
  negative = false
  secondpart = false
  val_exp = []
  current = 0 
  expected = [:Number, :Plus, :Minus]
  pos = 0
  tokens.each do |t|
    pos += t.to_s.size + 1 
    if t.numeric? and expected.include? :Number
      current = negative ^ secondpart ? -t.to_f : t.to_f
      expected = [:Mult, :End]
    elsif t == "*" and expected.include? :Mult 
      expected = [:Exp]
    elsif t[0] == "X" and t[1] == "^" and t[2..-1].numeric? and expected.include? :Exp
      negative = false
      if (t[2..-1].to_i > 9)
        exit_error("Exponent is way too high - limit is 9", equation, pos - t.size + 2)
      end
      val_exp += [[current, t[2..-1].to_i]]
      current = nil
      expected = [:Plus, :Minus, :Equal, :End]
    elsif t[0] == "X" and t[1] == "^" and t[2..-1].numeric? and expected.include? :Number
      negative = false
      if (t[2..-1].to_i > 9)
        exit_error("Exponent is way too high - limit is 9", equation, pos - t.size + 2)
      end
      val_exp += [[negative ^ secondpart ? -1 : 1, t[2..-1].to_i]]
      current = nil
      expected = [:Plus, :Minus, :Equal, :End]
    elsif t[0] == "X" and t[1] == nil and expected.include? :Exp
      negative = false
      val_exp += [[current, 1]]
      current = nil
      expected = [:Plus, :Minus, :Equal, :End]
    elsif t[0] == "X" and t[1] == nil and expected.include? :Number
      negative = false
      val_exp += [[negative ^ secondpart ? -1 : 1, 1]]
      current = nil
      expected = [:Plus, :Minus, :Equal, :End]
    elsif t == "-" and expected.include? :Minus 
      negative = true
      expected = [:Number]
    elsif t == "=" and !secondpart and expected.include? :Equal
      secondpart = true
      expected = [:Number, :Plus, :Minus]
    elsif t == "+" and expected.include? :Plus
      expected = [:Number]
    elsif t == "+" and expected.include? :Mult
      val_exp += [[current, 0]]
      current = nil
      expected = [:Number]
    elsif t == "-" and expected.include? :Mult
      val_exp += [[current, 0]]
      current = nil
      negative = true
      expected = [:Number]
    elsif t == "=" and !secondpart and expected.include? :Mult
      secondpart = true
      val_exp += [[current, 0]]
      current = nil
      expected = [:Number, :Plus, :Minus]
    else
      exit_error("Expected token #{expected.join(" or ")}, instead got '#{t}'", equation, pos - t.size - 1)
    end
  end
  val_exp += [[current, 0]] if current != nil
  if !expected.include? :End
    exit_error("Expected token #{expected.join(" or ")}, instead got nothing.", equation, equation.size)
  end
  ret = []
  val_exp.sort! {|a, b| b[1] <=> a[1]} #sort the array of [value, exp] by exp
  current = 0
  val_exp.size.times do |i|
    index = val_exp[i][1]
    ret[index] = 0 if ret[index] == nil
    ret[index] += val_exp[i][0]
  end
  ret = ret.map {|x| x == nil ? 0 : x}
  ret.pop until ret.last != 0
  ret = [0] if ret == []
  ret.each {|x| abort "#{x} is too extreme a number, please calm down." if x > @LIMIT or x < -@LIMIT}
  ret
end

def str_equation(terms)
  str = ""
  terms.size.times do |i|
    if (i == 0)
      str += " #{terms[0]} "
    elsif i == 1
      str += "+ #{terms[1]}X "
    else
      str += "+ #{terms[i]}X^#{i} "
    end
  end
  str + "= 0"
end

def sqrt(a)
  x = a
  y = 1.0
  e = 0.000001
  while x - y > e
    x = (x + y) / 2
    y = a / x
  end
  x
end
def diff(n, mid)  
  if (n > (mid * mid * mid))  
    return (n - (mid * mid * mid)) 
  else  
    return ((mid * mid * mid) - n) 
  end
end

def sqrt3(x)
  xn = x / 2.0
  100.times do
    xn = ((x / (xn * xn)) + 2 * xn) / 3
  end
  xn
end

equation = ARGV[0]
if equation == nil
  print "Please enter your equation >> "
  equation = gets.chomp
end
terms = get_equations_terms(equation)
puts "Reduced form: #{str_equation(terms)}"
degree = [terms.size - 1, 0].max
puts "Polynomial degree : #{degree}"
if (degree == 0)
  if terms[0] == 0
    puts "All real numbers are solution"
  else
    puts "This equation is impossible."
  end
elsif degree == 1
  puts "Solution is #{-terms[0]} / #{terms[1]} = #{-terms[0] / terms[1]}"
elsif (degree == 2)
  a = terms[0]
  b = terms[1]
  c = terms[2]
  delta = b * b - 4 * a * c
  puts "Discriminant is #{delta}"
  if (delta < 0)
    puts "Discriminant is strictly negative"
    puts "Two complex solutions : "
    solutionr = (b / (c * 2)).round(5)
    solutioni = (sqrt(-delta) / (c * 2)).round(5)
    puts "#{a}/#{c * 2} - #{sqrt(delta)}/#{2 * c} = #{solutionr} - #{solutioni}i"
    puts "#{a}/#{c * 2} + #{sqrt(delta)}/#{2 * c} = #{solutionr} + #{solutioni}i"
  elsif delta == 0
    puts "Discriminant is nul"
    puts "One real solution :"
    puts "#{-b} / (2 * #{a}) = #{-b / (2 * a)}"
  else
    puts "Discriminant is stricly positive"
    puts "Two real solutions : "
    puts "(#{-b} - sqrt(#{delta})) / 2 * #{c} = #{((-b - sqrt(delta)) / (2 * c)).round(5)}" 
    puts "(#{-b} + sqrt(#{delta})) / 2 * #{c} = #{((-b + sqrt(delta)) / (2 * c)).round(5)}" 
  end
else
  puts "Solution for equation of degree #{degree} isn't a feature yet"
end
